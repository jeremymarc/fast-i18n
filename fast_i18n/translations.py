import logging
import re
from collections import defaultdict
from dataclasses import dataclass, field

import deep_translator

from fast_i18n import Colors

logger = logging.getLogger(__name__)


@dataclass
class Wording:
    DUMMY_LETTERS_PATTERN = re.compile(r"[a-zéçèàùœêâîûôæëïüÿ]", re.IGNORECASE)
    DUMMY_CONTINUE_PATTERN = re.compile(r"(.[^<{]*)", re.DOTALL)
    DUMMY_TRANSLATE_DELIMITERS = dict(
        (delim, re.compile(pattern, re.DOTALL))
        for delim, pattern in {
            r"{{": r"^(\{\{.+?\}\})",
            r"{": r"^(\{[^\{\}]+\})",
            r"<": r"^(<[^<>]+>)",
        }.items()
    )
    COUNT_NON_WORD_PATTERN = re.compile(r"[^a-zéçèàùœêâîûôæëïüÿ\s-]", re.IGNORECASE)
    COUNT_EXCLUDED_PATTERNS = list(
        re.compile(pattern.pattern[1:], re.DOTALL)
        for delim, pattern in DUMMY_TRANSLATE_DELIMITERS.items()
    )

    # TODO: make languages generic instead of french based
    french: str = ""
    italian: str = ""
    italian_auto: dict = field(default_factory=dict)
    # TODO: deprecation by origin (a deprecated_origin set ?)
    origins: set = field(default_factory=set)
    key: str = ""
    module: str = ""
    deprecated: bool = False

    def dummy_translation(self):
        res = ""
        i = 0
        while i < len(self.french):
            string = self.french[i:]
            for delim, pattern in self.DUMMY_TRANSLATE_DELIMITERS.items():
                if string.startswith(delim):
                    match = re.match(pattern, string)
                    if match:
                        res += match.group()
                        i += len(match.group())
                        break
            else:
                match = re.match(self.DUMMY_CONTINUE_PATTERN, string)
                res += re.sub(self.DUMMY_LETTERS_PATTERN, "*", match.group())
                i += len(match.group())
        return res

    def count(self, language=None):
        """
        Returns the number of letters and words in the given language ignoring punctuation and beacons.
        Very useful for translators pricing!
        """
        if language is None:
            language = "french"
        text = getattr(self, language, self.french)
        # Removing HTML and variables
        for pattern in self.COUNT_EXCLUDED_PATTERNS:
            text = re.sub(pattern, "", text)
        # Removing punctuation and numbers
        text = re.sub(self.COUNT_NON_WORD_PATTERN, "", text)
        return len(text.replace(" ", "")), len(text.replace("-", " ").split(" "))

    def translate(self, auto_translate=False, dummy_translate=False):
        if dummy_translate:
            self.italian = self.dummy_translation()
        if not auto_translate or not self.french:
            return  # Do not use automatic translators
        for translator in Translator.TRANSLATORS:
            if self.italian_auto.get(translator.__class__.__name__, None):
                continue  # Already translated
            try:
                print(f" * Translating (using {translator.__class__.__name__}): {self.french}")
                self.italian_auto[translator.__class__.__name__] = translator.translate(self.french)
            except Exception as e:
                logger.warning(e)
                self.italian_auto[translator.__class__.__name__] = "-"

    @property
    def is_translated(self):
        return self.italian

    def check(self):
        if not self.french:
            return True
        errors = 0
        for pattern in self.COUNT_EXCLUDED_PATTERNS:
            errors += int(not self.check_pattern(pattern))
        return errors == 0

    def check_pattern(self, pattern, replace=True):
        ref = pattern.findall(self.french)
        if not len(ref):
            return True
        trans = pattern.findall(self.italian)
        if trans != ref:
            if not replace or len(trans) != len(ref):
                logger.error(
                    "********** %s **********\n" + "Ref:   %s\n" + "Trans: %s\n",
                    self.key,
                    ref,
                    trans,
                )
                return False
            if len(trans) == len(ref):
                for t, r in zip(trans, ref):
                    self.italian = self.italian.replace(t, r)
            logger.warning(
                "!!!!!!!!!! %s !!!!!!!!!!\nRef:   %s\nTrans: %s\nFix: %s\n",
                self.key,
                ref,
                trans,
                self.italian,
            )
            return self.check_pattern(pattern, replace=False)
        return True


def find_code(codes, supported_languages, translator_name):
    for code in codes:
        if code in supported_languages:
            return code
    raise Exception(
        f"Target language '{codes}' not supported by translator '{translator_name}'\nSupported languages are:\n - "
        + "\n - ".join(supported_languages)
    )


def generate_translator(translator_name, source_codes, target_codes):
    print(f" + Registering translator '{translator_name}'")
    cls = getattr(deep_translator, translator_name)
    supported_languages = cls.get_supported_languages()
    source = find_code(source_codes, supported_languages, translator_name)
    target = find_code(target_codes, supported_languages, translator_name)
    # proxies_example = {
    #     "https": "34.195.196.27:8080",
    #     "http": "34.195.196.27:8080"
    # }
    return cls(source=source, target=target)  # , proxies=proxies_example


class Translator:
    IMMUTABILITY_CHECK: dict = defaultdict(Wording)
    WORDINGS: dict = defaultdict(Wording)
    HEADER_ROW_NB: int = 1
    EXCEL_MAX_CELL_LENGTH: int = 32767
    TRANSLATORS = [
        generate_translator(translator_name, source_codes=("fr", "french"), target_codes=("italian", "it"))
        for translator_name in (
            "GoogleTranslator",
            # "MicrosoftTranslator",  # API key needed
            "PonsTranslator",
            # "LingueeTranslator",  # Nul
            # "MyMemoryTranslator",  # Nul
            # "YandexTranslator",  # API key needed
            # "PapagoTranslator",  # Removed ?
        )
    ]

    def __init__(self, mutable=True) -> None:
        super().__init__()
        self.mutable = mutable

    def ensure_immutability(self, old, new):
        if old.french.replace("\n", "").replace(" ", "") != new.french.replace("\n", "").replace(" ", ""):
            origins = ", ".join(new.origins)
            raise Exception(
                f"Reference french wording '{Colors.Y}{old.french}{Colors.ENDC}' has changed to '{Colors.R}{new.french}{Colors.ENDC}' in immutable translator ({origins})."
            )

    def add(self, wording, pool=False):
        if wording.key in self.WORDINGS:
            final = self.WORDINGS[wording.key]
            if pool is False and wording.origins != final.origins:
                raise Exception(f"Collision on wording '{wording.key}': {final.origins} != {wording.origins}")
            final.origins = final.origins.union(wording.origins)
            final.deprecated = wording.deprecated
            if self.mutable is False:
                self.ensure_immutability(final, wording)
            else:
                final.french = wording.french
            return
        self.WORDINGS[wording.key] = wording

    def translate(self, target=None, auto_translate=False, dummy_translate=False):
        for wording in self.WORDINGS.values():
            if target is not None:
                for origin in wording.origins:
                    if target in origin:
                        break
                else:
                    continue
            wording.translate(auto_translate=auto_translate, dummy_translate=dummy_translate)

    def check(self, statistics=None):
        untranslated = invalid = valid = 0
        for wording in self.WORDINGS.values():
            if not wording.is_translated:
                untranslated += 1
                continue
            if not wording.check():
                invalid += 1
            else:
                valid += 1
        if statistics is not None:
            statistics["valid"] = valid
            statistics["invalid"] = invalid
            statistics["untranslated"] = untranslated
