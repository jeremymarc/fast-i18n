import logging
import os
import re
import time
from dataclasses import dataclass, field
from typing import Any

from fast_i18n import Colors
from fast_i18n.translations import Wording

try:
    from bs4 import BeautifulSoup, Tag, DEFAULT_OUTPUT_ENCODING
    from bs4.element import Comment, NavigableString

    # Ugly: patching bs4.prettify which doesn't provide an indent parameter.
    # A PR is waiting with this feature for ages...
    original_decode = Tag.decode
    original_decode_contents = Tag.decode_contents

    def patched_decode(self, indent_level=None, eventual_encoding=DEFAULT_OUTPUT_ENCODING, formatter="minimal"):
        if indent_level is not None and indent_level % 2 == 0 and indent_level > 1:
            # Double indent_level incrementation starting level 2
            indent_level += 1
        return original_decode(self, indent_level=indent_level, eventual_encoding=eventual_encoding, formatter=formatter)
    
    def patched_decode_contents(self, indent_level=None, eventual_encoding=DEFAULT_OUTPUT_ENCODING, formatter="minimal"):
        if indent_level is not None and indent_level % 2 == 0 and indent_level > 1:
            # Double indent_level incrementation starting level 2
            indent_level += 1
        return original_decode_contents(self, indent_level=indent_level, eventual_encoding=eventual_encoding, formatter=formatter)

    Tag.decode = patched_decode
    Tag.decode_contents = patched_decode_contents
except ImportError:
    pass

logger = logging.getLogger(__name__)


class Processor:
    def __init__(self, translator, verbose=1) -> None:
        self.translator = translator
        self.verbose = verbose

    def extract(self):
        raise NotImplementedError()

    def update(self, wordings, language):
        raise NotImplementedError()

    def log(self, *args, level=0, **kwargs):
        if self.verbose >= level:
            print(*args, **kwargs)


@dataclass
class DjangoTable:
    apps: Any
    module_name: str
    attributes: list
    filters: dict = field(default_factory=dict)
    sub_tables: dict = field(default_factory=dict)
    pool: bool = True

    @property
    def model_name(self):
        return self.module_name.split(".")[-1]

    @property
    def model(self):
        return self.apps.get_model(*self.module_name.split("."))

    @property
    def queryset(self):
        return self.model.objects.filter(**self.filters).prefetch_related(*self.sub_tables.keys()).order_by("pk")


@dataclass
class DjangoDatabaseProcessor(Processor):
    def __init__(self, translator, tables, apps, **kwargs):
        super().__init__(translator, **kwargs)
        self.django_apps = apps
        self.tables = tables

    def get_module_name(self, instance):
        return f"{instance._meta.app_label}.{instance._meta.object_name}"

    def clean_string(self, value):
        value = value or ""
        return value.strip()

    def add_wording(self, table, pk, attr, text):
        text = self.clean_string(text)
        origin = f"{table.module_name}:{pk}:{attr}"
        self.translator.add(
            Wording(
                key=text if table.pool else origin,
                french=text,
                origins={origin},
                module=table.module_name,
            ),
            pool=table.pool,
        )

    def extract_instance(self, obj, table):
        for attr in table.attributes:
            value = getattr(obj, attr, None) or ""
            value = value.strip()
            if not value:
                continue
            if type(value) is not str:
                raise Exception(f"Invalid data type {type(value)} on {table.module_name}:{obj.pk}:{attr}")
            self.add_wording(table, obj.pk, attr, value)

    def extract_table(self, table):
        for instance in table.queryset:
            self.extract_instance(instance, table)
            if len(table.sub_tables):
                for relation, sub_table in table.sub_tables.items():
                    sub_queryset = getattr(instance, relation)
                    for sub_instance in sub_queryset.all().order_by("pk"):
                        self.extract_instance(sub_instance, sub_table)

    def extract(self):
        for table in self.tables:
            print(f" * Extracting model '{table.model_name}' ({table.queryset.count()} rows)")
            for relation, sub_table in table.sub_tables.items():
                print(f"   - Extracting sub-model '{sub_table.model_name}' ({sub_table.queryset.count()} rows)")
            self.extract_table(table)
            print("", flush=True)

    def update(self, language, statistics=None):
        wordings = self.translator.WORDINGS.values()
        self.log(
            f"{Colors.B}Updating django database with {Colors.C}{len(wordings)}{Colors.B} wordings in language {Colors.C}{language}{Colors.ENDC}",
            level=1,
        )
        for wording in wordings:
            self.log(f"Updating {Colors.R}{wording.key}{Colors.ENDC}... ({len(wording.origins)} wordings)", level=4)
            for origin in wording.origins:
                module_name, pk, attr = origin.split(":")
                model = self.django_apps.get_model(*module_name.split("."))
                value = {attr: wording.italian if language in ("it", "italian") else wording.french}
                self.log(f"{Colors.Y}{origin} = {Colors.ENDC}{value[attr]}{Colors.ENDC}", level=5)
                model.objects.filter(pk=pk).update(**value)
                if statistics is not None:
                    statistics[model.__name__] += 1


class DirectoryProcessor(Processor):
    EXTENSION: str

    def __init__(self, translator, folder, root=None, excluded_files=None, excluded_folders=None, included_files=None, verbose=1):
        super().__init__(translator, verbose=verbose)
        if root is None:
            root, folder = folder, "."
        root = os.path.realpath(root)
        if root[-1] != os.path.sep:
            root += os.path.sep
        if folder.startswith(root):
            folder = folder[len(root) :]  # noqa
        self.root = root
        self.folder = folder
        os.chdir(root)
        self.excluded_files = excluded_files or []
        self.excluded_folders = excluded_folders or []
        self.included_files = included_files

    @property
    def next_unique_id(self):
        self._unique_id += 1
        return self._unique_id - 1

    def extract(self, update_files=False):
        self.log(f"{Colors.C}Starting to search in {Colors.B}{self.folder}{Colors.ENDC}", level=1)
        skip = False
        total_count = 0
        self._unique_id = int(time.time() * 1000)  # epoch (in ms)
        # traverse root directory, and list directories as dirs and files as files
        for root, dirs, files in os.walk(self.folder):
            for folder in self.excluded_folders:
                if folder in root:
                    skip = True
                    break
            if skip:
                skip = False
                self.log(f"{Colors.R}{root}: {Colors.Y}skipped{Colors.ENDC}", level=3)
                continue
            for file in files:
                if not file.endswith(self.EXTENSION):
                    continue
                self.filepath = os.path.join(root, file)
                if self.included_files is not None:
                    if file not in self.included_files:
                        self.log(f"{Colors.R}{self.filepath}: {Colors.Y}skipped{Colors.ENDC}", level=3)
                        continue
                if file in self.excluded_files:
                    self.log(f"{Colors.R}{self.filepath}: {Colors.Y}skipped{Colors.ENDC}", level=3)
                    continue
                words_count = self.process_file(update_files=update_files)
                self.log(f"{Colors.R}{self.filepath}: {Colors.Y}{words_count}{Colors.ENDC}", level=1)
                total_count += words_count
        self.log(f"{Colors.C}Total word count {Colors.B}{total_count}{Colors.ENDC}", level=1)


class HtmlTemplateProcessor(DirectoryProcessor):
    EXTENSION = ".html"
    HTML_PREFIX = "I18N-"
    HTML_SUFFIX = ""
    HTML_FORMATTER = None  # "html5"
    HTML_I18N_ATTRIBUTES = ["title", "caption", "placeholder", "alt"]
    SIZE_WARNING = 99999
    # Do not parse these beacons
    BEACON_SKIPPED = ["script"]
    # These beacons are text dependant and will appear in translations
    BEACON_INCLUDED = ["a", "b", "i", "u", "br", "img", "span", "time", "small", "strong"]
    # These patterns are excluded from translations
    CLEAN_TEXT_PATTERNS = [
        (re.compile(p, re.IGNORECASE | re.DOTALL), s)
        for p, s in {
            "[ \n]* +": " ",  # Merge multiple spaces
        }.items()
    ]
    JINJA_PATTERNS = [
        (re.compile(p, re.IGNORECASE | re.DOTALL), s)
        for p, s in {
            r"\{\{.+?\}\}": "",  # Ignore single variables
            r"<%.*?%>": "",  # Remove blocks
            r"\{%.*?%\}": "",  # Remove blocks
            r"<#.*?#>": "",  # Remove comments
            r"\{#.*?#\}": "",  # Remove comments
        }.items()
    ]

    def clean_text(self, text):
        for pattern, replacement in self.CLEAN_TEXT_PATTERNS:
            text = re.sub(pattern, replacement, text)
        return text.strip()

    def is_jinja(self, text):
        for pattern, replacement in self.JINJA_PATTERNS:
            text = re.sub(pattern, replacement, text)
        return text.strip() == ""

    def extract_content(self, elt, append_children=False):
        nb_strings = nb_excluded = nb_included = 0
        children = []
        res = ""
        for child in elt.children:
            if isinstance(child, Comment):
                # Comment
                nb_excluded += 1
                self.log(f"{Colors.G}Comment{Colors.ENDC} skipped ({child})", level=5)
                continue
            if isinstance(child, NavigableString):
                # Simple text
                text = self.clean_text(str(child))
                if not text:
                    continue
                if self.is_jinja(text):
                    # This text must be included but should not be translated. Let's make it count as a beacon.
                    res += text
                    nb_excluded += 1
                    continue
                self.log(f"{Colors.G}Text{Colors.ENDC} added ({text})", level=5)
                nb_strings += 1
                res += text
            elif child.name in self.BEACON_INCLUDED:
                # Beacon inlcuded in translations
                text = self.clean_text(str(child))
                self.log(f"{Colors.G}Included{Colors.ENDC} added ({text})", level=5)
                nb_included += 1
                children.append(child)
                res += text
            else:
                # Beacon excluded from translations
                self.log(f"{Colors.G}Excluded{Colors.ENDC} added ({child.name})", level=5)
                nb_excluded += 1
                children.append(child)
        if nb_strings == 0:
            if append_children:
                self.todo_elts = children + self.todo_elts
            return "", "skipped"
        elif nb_included >= 0 and nb_excluded == 0:
            return res, "content"
        raise Exception(
            f"Mixing text and content\n"
            f"{Colors.R} - file:{Colors.ENDC} {self.filepath}\n"
            f"{Colors.R} - element:{Colors.ENDC} {elt.name} ({nb_included} included - {nb_excluded} excluded)\n"
            f"{Colors.R} - text:{Colors.ENDC} {res} ({nb_strings} strings)\n"
            f"{Colors.R} - content:{Colors.ENDC} {str(elt)}"
        )

    def extract_attr(self, i18n_key, i18n_id=None, element=None):
        if element is None:
            kwargs = {i18n_key: i18n_id}
            element = self.soup.find(**kwargs)
        attr = i18n_key[len(self.HTML_PREFIX) : -len(self.HTML_SUFFIX)]  # noqa
        if attr in ("text", "content"):
            value, attr = self.extract_content(element)
        else:
            value = element.get(attr) or ""
            if self.is_jinja(value):
                value = ""
        return value.strip()

    def set_attr(self, element, attribute="text"):
        key = f"{self.HTML_PREFIX}{attribute}{self.HTML_SUFFIX}"
        if key in element.attrs:
            # Inplace translation update
            unique_id = element[key]
            new_translation = False
        else:
            # New translation
            unique_id = self.next_unique_id
            new_translation = True
        value = self.extract_attr(key, i18n_id=unique_id, element=element)
        if not value:
            return ""
        if new_translation:
            element[key] = unique_id
            self.log(f"{Colors.Y}{key}:{unique_id} {Colors.ENDC}{value}", level=2)
        filepath = self.filepath.replace(self.root, "")
        origin = f"{filepath}:{key}:{unique_id}"
        self.translator.add(Wording(french=value, origins={origin}, key=origin, module=filepath), pool=False)
        return value

    def process_element(self, elt):
        if elt.name in self.BEACON_SKIPPED:
            self.log(f"{Colors.R}{elt.name} {Colors.C}SKIPPED{Colors.ENDC}", level=4)
            return
        self.log(f"{Colors.R}{elt.name} {Colors.C}{[e.name for e in self.todo_elts]}{Colors.ENDC}", level=4)
        for attr in self.HTML_I18N_ATTRIBUTES:
            if attr in elt.attrs:
                text = self.set_attr(elt, attr)
                self.words_count += len(text.split())
        text, attr = self.extract_content(elt, append_children=True)
        if text:
            self.set_attr(elt, attribute=attr)
            self.words_count += len(text.split())

    def process_file(self, update_files=False):
        self.words_count = 0
        with open(self.filepath, "r") as fd:
            source = fd.read()
        self.soup = BeautifulSoup(source, "html.parser")
        self.todo_elts = list(self.soup.find_all(recursive=False))
        while len(self.todo_elts):
            elt = self.todo_elts.pop(0)
            self.process_element(elt)
        if update_files:
            with open(self.filepath, "w") as fd:
                fd.write(self.soup.prettify(formatter=self.HTML_FORMATTER))
        return self.words_count

    def get_soup(self, source_soups):
        if self.filepath not in source_soups:
            with open(self.filepath, "r") as fd:
                source = fd.read()
            soup = BeautifulSoup(source, "html.parser")
            source_soups[self.filepath] = soup
        self.soup = source_soups[self.filepath]

    def update(self, language, statistics=None, skip_deprecated=False):
        source_soups = dict()
        for wording in self.translator.WORDINGS.values():
            translation = getattr(wording, language)
            for origin in wording.origins:
                self.filepath, key, unique_id = origin.split(":")
                self.get_soup(source_soups)
                results = self.soup.find_all(attrs={key: unique_id})
                if len(results) > 1:
                    raise Exception(f"Multiple values for identifier {key}={unique_id} in file {self.filepath}")
                if len(results) == 0:
                    if skip_deprecated:
                        self.log(f"Can't find {Colors.Y}{key}=\"{unique_id}\"{Colors.ENDC} in {Colors.R}{self.filepath}{Colors.ENDC}:\n{Colors.C}{wording.french}{Colors.ENDC}")
                        continue
                    raise Exception(f"Can't find {Colors.Y}{key}=\"{unique_id}\"{Colors.ENDC} in {Colors.R}{self.filepath}{Colors.ENDC}:\n{Colors.C}{wording.french}{Colors.ENDC}")
                elt = results[0]
                attr = key[len(self.HTML_PREFIX) : -len(self.HTML_SUFFIX)]  # noqa
                if attr in ("text", "content"):
                    elt.clear()
                    tree = BeautifulSoup(translation, "html.parser")
                    elt.extend(tree)
                else:
                    elt[attr] = translation
                if statistics is not None:
                    statistics[self.filepath] += 1
        # Writing data
        for filepath, soup in source_soups.items():
            self.log(f"Writing {Colors.R}{filepath}{Colors.ENDC}...", level=2)
            source = soup.prettify(formatter=self.HTML_FORMATTER)
            if len(source) > self.SIZE_WARNING:
                self.log(f"{Colors.Y}{filepath} {Colors.R}IS BIG {Colors.ENDC} ({len(source)})", level=2)
            with open(filepath, "w") as fd:
                fd.write(source)


class JavascriptProcessor(DirectoryProcessor):
    EXTENSION = ".js"
    JS_I18N_PREFIX = r"/* I18N:{unique_id} */"
    JS_I18N_PATTERN = r"/\* I18N:(?P<unique_id>\d+) \*/"
    JS_STRING_PATTERN = r"(?P<quote>['\"])(?P<string>.*?)(?<!\\)(?P=quote)"
    JS_FULL_PATTERN = re.compile(f"({JS_I18N_PATTERN})?" + JS_STRING_PATTERN, re.DOTALL)

    def user_accepts(self, line, match):
        start, end = match.span()
        preview = Colors.Y + line[:start] + Colors.B + match.group() + Colors.Y + line[end:]
        self.log(
            "Do you want to add this translation ?\n"
            + f" - file: {Colors.R}{self.filepath}{Colors.ENDC}\n"
            + f" - preview: {preview}{Colors.ENDC}\n",
            level=0,
        )
        val = input("> ")
        return val.lower() in ("y", "yes", "o", "oui")

    def process_match(self, line, match):
        value, unique_id, quote = match["string"], match["unique_id"], match["quote"]
        if not value:
            return line
        filepath = self.filepath.replace(self.root, "")
        if unique_id is None:
            # New translation
            if not self.user_accepts(line, match):
                return line
            unique_id = self.next_unique_id
            self.log(f"{Colors.Y}{filepath}:{unique_id} {Colors.ENDC}{value}", level=2)
        origin = f"{filepath}:{unique_id}"
        self.translator.add(Wording(french=value, origins={origin}, key=origin, module=filepath), pool=False)
        prefix = self.JS_I18N_PREFIX.format(unique_id=unique_id)
        self.words_count += len(value.split())
        return line[: match.start()] + f"{prefix}{quote}{value}{quote}" + line[match.end() :]  # noqa

    def process_file(self, update_files=False):
        self.words_count = 0
        with open(self.filepath, "r") as fd:
            source = fd.read()
        lines = []
        for line in source.split("\n"):
            # Reversed to prevent last match indexes to be invalid once the precedent are replaced
            matches = reversed(list(self.JS_FULL_PATTERN.finditer(line)))
            for match in matches:
                line = self.process_match(line, match)
            if update_files:
                lines.append(line)
        if update_files:
            with open(self.filepath, "w") as fd:
                fd.write("\n".join(lines))
        return self.words_count

    def get_source(self, source_files):
        if self.filepath not in source_files:
            with open(self.filepath, "r") as fd:
                source_files[self.filepath] = fd.read()
        return source_files[self.filepath]

    def update(self, language, statistics=None):
        source_files = dict()
        for wording in self.translator.WORDINGS.values():
            translation = getattr(wording, language)
            for origin in wording.origins:
                self.filepath, unique_id = origin.split(":")
                source = self.get_source(source_files)
                tag = self.JS_I18N_PREFIX.format(unique_id=unique_id)
                pattern = re.compile(re.escape(tag) + self.JS_STRING_PATTERN, re.MULTILINE)
                matches = list(pattern.finditer(source))
                if len(matches) > 1:
                    raise Exception(f"Multiple values for identifier {tag} in file {self.filepath}")
                if len(matches) == 0:
                    raise Exception(f"Can't find identifier {tag} in file {self.filepath}")
                match = matches[0]
                start, end = match.span()
                quote = match["quote"]
                replacement = f"{tag}{quote}{translation}{quote}"
                source = source[:start] + replacement + source[end:]
                source_files[self.filepath] = source
                if statistics is not None:
                    statistics[self.filepath] += 1
        # Writing data
        for filepath, source in source_files.items():
            self.log(f"Writing {Colors.R}{filepath}{Colors.ENDC}...", level=2)
            with open(filepath, "w") as fd:
                fd.write(source)
