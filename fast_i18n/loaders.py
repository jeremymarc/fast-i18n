import logging
import os
import re
from copy import copy

import openpyxl
from openpyxl.styles import Alignment, Font

from fast_i18n.translations import Wording

logger = logging.getLogger(__name__)


class Loader:
    def __init__(self, translator):
        self.translator = translator

    def load(self):
        raise NotImplementedError()

    def dump(self):
        raise NotImplementedError()


class XlsxLoader(Loader):
    SHEET_NAME = "translations"
    HEADER_ROW_NB = 1
    EXCEL_MAX_CELL_LENGTH = 32767
    TRANSLATION_COLUMN_WIDTH = 80
    ROW_HEIGHT = 14
    TRANSLATION_FONT = Font(name="FreeMono")

    def __init__(self, translator, filepath, **kwargs):
        super().__init__(translator, **kwargs)
        self.filepath = filepath

    def load(self, deprecate_all):
        if not self.filepath or not os.path.exists(self.filepath):
            raise Exception(f"Invalid input file '{self.filepath}'")
        workbook = openpyxl.load_workbook(filename=self.filepath)
        sheet = workbook.active
        # Headers
        self.translator_columns = dict((sheet[chr(ord("A") + c) + "1"].value, chr(ord("A") + c)) for c in range(5, sheet.max_column))
        # Content
        for row in range(self.HEADER_ROW_NB + 1, sheet.max_row + 1):
            key = sheet[f"A{row}"].value or ""
            module = sheet[f"B{row}"].value or ""
            try:
                origins = set(sheet[f"C{row}"].value.split("\n"))
            except Exception as e:
                raise Exception(f"Fail to extract origin in row {row}") from e
            french = sheet[f"D{row}"].value or ""
            italian = sheet[f"E{row}"].value or ""
            # TODO: automatic translations should be stored in a smarter way
            italian_auto = dict((translator_name, sheet[f"{column}{row}"].value) for translator_name, column in self.translator_columns.items())
            wording = Wording(
                key=key, module=module, french=french, italian=italian, origins=origins, italian_auto=italian_auto, deprecated=deprecate_all
            )
            first_origin = next(iter(origins))
            if first_origin.startswith("recipe_mgr.Recipe:") or first_origin.startswith("recipe_mgr.RecipeInstruction:"):
                # legacy
                assert len(origins) == 1
                wording.key = first_origin
                self.translator.add(wording, pool=False)
            else:
                self.translator.add(wording, pool=True)

    def write_headers(self):
        headers = Wording(
            "French",
            "Italian",
            key="Key",
            module="Module",
            origins=set(["Origin"]),
            italian_auto=dict((c, c) for c in self.translator_columns.keys()),
        )
        self.write_wording(headers)
        # Style
        for col in "DEFG":
            column = self.sheet.column_dimensions[col]
            column.width = self.TRANSLATION_COLUMN_WIDTH
            column.bestFit = True
            column.font = self.TRANSLATION_FONT
        for col in "AB":
            self.sheet.column_dimensions[col].hidden = True

    def apply_row_style(self, wording):
        row = self.sheet.row_dimensions[self.row]
        french_height = sum(1 + (len(line) - 1) // self.TRANSLATION_COLUMN_WIDTH for line in wording.french.split("\n"))
        italian_height = sum(1 + (len(line) - 1) // self.TRANSLATION_COLUMN_WIDTH for line in wording.italian.split("\n"))
        row.height = max(french_height, italian_height) * self.ROW_HEIGHT
        font = copy(self.TRANSLATION_FONT)
        if wording.deprecated:
            font.color = "777777"
            font.italic = True
        row.font = font

    def apply_cell_style(self, wording, cell):
        cell.alignment = Alignment(wrapText=True)
        font = copy(self.TRANSLATION_FONT)
        if wording.deprecated:
            font.color = "777777"
            font.italic = True
            # cell.fill = Fill(fill_type=Fill.FILL_SOLID, start_color="bbbbbb")
        if not wording.italian and not wording.deprecated:
            font.bold = True
        cell.font = font

    def write_origin_batch(self, wording, origins):
        if len(origins) >= self.EXCEL_MAX_CELL_LENGTH:
            raise Exception(f"Wording '{wording.key}' exceeds origin max size ({len(origins)} > {self.EXCEL_MAX_CELL_LENGTH})")
        self.sheet.cell(column=1, row=self.row, value=wording.key)
        self.sheet.cell(column=2, row=self.row, value=wording.module)
        self.sheet.cell(column=3, row=self.row, value=origins)
        self.apply_cell_style(wording, self.sheet.cell(column=4, row=self.row, value=wording.french))
        self.apply_cell_style(wording, self.sheet.cell(column=5, row=self.row, value=wording.italian))
        self.apply_row_style(wording)
        for translator_name, italian in wording.italian_auto.items():
            if translator_name not in self.translator_columns:
                # This translator seems to have been removed
                logger.error(f"Translator removed {translator_name}")
                continue
            self.sheet.cell(column=self.translator_columns[translator_name], row=self.row, value=italian)
        self.row += 1

    def write_wording(self, wording, statistics=None):
        if statistics is not None:
            letters, words = wording.count("french")
            statistics["character_count"] += letters
            statistics["word_count"] += words
            statistics["sentences_count"] += 1
        origins = ""
        for origin in wording.origins:
            """
            The batch processing is required because Excel has a hard limit for cell length (EXCEL_MAX_CELL_LENGTH).
            """
            if len(origins) + len(origin) + 1 >= self.EXCEL_MAX_CELL_LENGTH:
                self.write_origin_batch(wording, origins)
                origins = origin
            else:
                if origins:
                    origins += "\n"
                origins += origin
        if origins:
            self.write_origin_batch(wording, origins)

    def dump(self, statistics=None):
        self.translator_columns = dict((translator.__class__.__name__, i + 6) for i, translator in enumerate(self.translator.TRANSLATORS))
        workbook = openpyxl.Workbook()
        self.sheet = workbook.active
        self.sheet.name = self.SHEET_NAME
        # Header
        self.row = self.HEADER_ROW_NB
        self.write_headers()
        # Content
        for wording in self.translator.WORDINGS.values():
            self.write_wording(wording, statistics)
        workbook.save(self.filepath)
