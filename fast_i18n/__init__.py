__title__ = "fast-i18n"
__version__ = "0.0.1"
__license__ = "MIT"
__copyright__ = "Copyright 2021"

# Version synonym
VERSION = __version__


class Colors:
    P = PINK = "\033[95m"
    B = BLUE = "\033[94m"
    C = CYAN = "\033[96m"
    G = GREEN = "\033[92m"
    Y = YELLOW = "\033[93m"
    R = RED = "\033[91m"
    E = END = ENDC = "\033[0m"
    BOLD = "\033[1m"
    UNDERLINE = "\033[4m"
